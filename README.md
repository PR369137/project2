
<html>
  <head>
    <style>
    
    #div1 {
        width: 350px;
        height: 70px;
        padding: 10px;
        border: 1px solid #aaaaaa;
    }
    
    img {
      position: relative;
    }
    
    
    #drag1{
    width :100px;
    
    
    }
    </style>
<script>
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
</script>
<body>

<svg width="200" height="200">
  <circle cx="100" cy="100" r="80"
  stroke="black" stroke-width="3" fill="yellow" />
  </svg>
<svg width="200" height="200">
  <circle cx="100" cy="100" r="80"
  stroke="black" stroke-width="3" fill="green" />
</svg>
<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
<br>
<img id="drag1" src="logo.gif" draggable="true" ondragstart="drag(event)" width="100" height="100">

 <p>Click here to get current location.</p>

<button onclick="getLocation()">Location</button>

<p id="abc"></p>

<script>
var x = document.getElementById("abc");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude;
}
</script>
</body>
</html>